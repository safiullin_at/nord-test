<?php

class Node
{
    private $_id;
    private $_parent;
    private $_name;

    public function __construct($name)
    {
        $this->_name = $name;
    }

    public function getName() {
        return $this->_name;
    }

    public function setParent($parent) {
        $this->_parent = $parent;
    }

    public function getParent() {
        return $this->_parent;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getId() {
        return $this->_id;
    }
}

abstract class BaseTree
{
    // создает узел (если $parentNode == NULL - корень)
    abstract protected function createNode(Node $node, Node $parentNode = null);

    // удаляет узел и все дочерние узлы
    abstract protected function deleteNode(Node $node);

    // один узел делает дочерним по отношению к другому
    abstract protected function attachNode(Node $node, Node $parent);

    // получает узел по названию
    abstract protected function getNode($nodeName);

    // преобразует дерево со всеми элементами в ассоциативный массив
    abstract protected function export();

}

class Tree extends BaseTree
{
    private $_tree = array();
    private $_counterId = 0;

    private function _nodeKey(Node $node) {
        foreach($this->_tree as $_key => $_node) {
            if($node === $_node) {
                return $_key;
            }
        }
        return null;
    }

    public function createNode(Node $node, Node $parentNode = null) {
        $id = ++$this->_counterId;
        $node->setId($id);
        if(is_null($parentNode)) {
            $node->setParent(0);
        } else {
            $node->setParent($parentNode->getId());
        }
        $this->_tree[$id] = $node;
    }

    public function deleteNode(Node $node) {
        if($nodeKey = $this->_nodeKey($node)) {
            unset($this->_tree[$nodeKey]);
            foreach($this->_tree as $_key => $_node) {
                $parent = $_node->getParent();
                if($parent !== 0 && !isset($this->_tree[$parent])) {
                    unset($this->_tree[$_key]);
                }
            }
            return true;
        }
        return false;
    }

    public function attachNode(Node $node, Node $parentNode) {
        $node->setParent($parentNode->getId());
        return true;
    }

    public function getNode($nodeName) {
        foreach($this->_tree as $_node) {
            if($_node->getName() === $nodeName) {
                return $_node;
            }
        }
        return null;
    }

    private function _toArray($tree) {
        $arrayTree = array();
        if(!empty($tree)) {
            foreach($tree as $_id => $_node) {
                $arrayTree[$_id] = array(
                    'id' => $_node->getId(),
                    'parent' => $_node->getParent(),
                    'name' => $_node->getName()
                );
            }
        }
        return $arrayTree;
    }

    public function export() {
        $tree = $this->_toArray($this->_tree);
        $rootId = 0;
        if(!empty($tree)) {
            foreach ($tree as $_id => $_node) {
                if ($_node['parent']) {
                    $tree[$_node['parent']]['children'][$_id] =& $tree[$_id];
                } else {
                    $rootId = $_id;
                }
            }
            return array($rootId => $tree[$rootId]);
        }
        return null;
    }
}

$tree = new Tree;
// 1. создать корень country
$tree->createNode(new Node('country'));
// 2. создать в нем узел kiev
$tree->createNode(new Node('kiev'), $tree->getNode('country'));
// 3. в узле kiev создать узел kremlin
$tree->createNode(new Node('kremlin'), $tree->getNode('kiev'));
// 4. в узле kremlin создать узел house
$tree->createNode(new Node('house'), $tree->getNode('kremlin'));
// 5. в узле kremlin создать узел tower
$tree->createNode(new Node('tower'), $tree->getNode('kremlin'));
// 4. в корневом узле создать узел moskow
$tree->createNode(new Node('moskow'), $tree->getNode('country'));
// 5. сделать узел kremlin дочерним узлом у moskow
$tree->attachNode($tree->getNode('kremlin'), $tree->getNode('moskow'));
// 6. в узле kiev создать узел maidan
$tree->createNode(new Node('maidan'), $tree->getNode('kiev'));
// 7. удалить узел kiev
$tree->deleteNode($tree->getNode('kiev'));
// 8. получить дерево в виде массива, сделать print_r
print_r($tree->export());
