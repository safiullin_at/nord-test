1. Не использовать символ «*» в запросе, указав только те поля, которые нужны.
2. Использовать механизм JOIN
3. Повесить раздельные индексы на поля `info_id` и `data_id` таблицы `link`

Пример запроса
SELECT
	`d`.`id` as `d_id`,
	`d`.`date`,
	`d`.`value`,
	`i`.`id` as `i_id`,
	`i`.`name`,
	`i`.`desc`
FROM
	`data` as `d`
INNER JOIN
	`link` as `l`
INNER JOIN
	`info` as `i`
WHERE
	`l`.`info_id` = `i`.`id`
	AND
	`l`.`data_id` = `d`.`id`